core = 7.x
api = 2

; UW Endowment Application
projects[uw_endowment_app][type] = "module"
projects[uw_endowment_app][download][type] = "git"
projects[uw_endowment_app][download][url] = "https://git.uwaterloo.ca/wcms/uw_endowment_app.git"
projects[uw_endowment_app][download][tag] = "7.x-1.8"
projects[uw_endowment_app][subdir] = ""

; UW PHP APPS API
projects[uw_php_apps_api][type] = "module"
projects[uw_php_apps_api][download][type] = "git"
projects[uw_php_apps_api][download][url] = "https://git.uwaterloo.ca/wcms/uw_php_apps_api.git"
projects[uw_php_apps_api][download][tag] = "7.x-1.5"
projects[uw_php_apps_api][subdir] = ""

; UW Tuition Benefit Application
projects[uw_tuition_benefit_app][type] = "module"
projects[uw_tuition_benefit_app][download][type] = "git"
projects[uw_tuition_benefit_app][download][url] = "https://git.uwaterloo.ca/wcms/uw_tuition_benefit_app.git"
projects[uw_tuition_benefit_app][download][tag] = "7.x-1.18"
projects[uw_tuition_benefit_app][subdir] = ""

; UW Student Fee Estimator
projects[uw_student_fee_estimator][type] = "module"
projects[uw_student_fee_estimator][download][type] = "git"
projects[uw_student_fee_estimator][download][url] = "https://git.uwaterloo.ca/wcms/uw_student_fee_estimator.git"
projects[uw_student_fee_estimator][download][tag] = "7.x-1.15"
projects[uw_student_fee_estimator][subdir] = ""

